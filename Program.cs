﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XcDownLoadFile;

namespace WinPhoneFW
{
    class PackageFileInfo
    {
        public String ID;
        public long fileSize;
        public string filetype;
        public string filename;
        public string checksum;
        public string checktype;
        public int error;
        public long cursize;
        public String currentDir;
        public string URL;
        public string DownloadURL;
        public string sVersion;
        public PackageFileInfo(string id)
        {
            ID = id;
            fileSize = 1;
            error = 1;
            cursize = 0;
        }
    }
    class Program
    {
        //static public ConcurrentQueue<PackageFileInfo> g_fpinfo = new ConcurrentQueue<PackageFileInfo>();
        static public ConcurrentQueue<PackageFileInfo> g_downfiles = new ConcurrentQueue<PackageFileInfo>();
        static public int ThreadCount = 0;
        static void GetDownloadURL(Object obj)
        {
            Interlocked.Add(ref ThreadCount, 1);
            PackageFileInfo pif = (PackageFileInfo)obj;
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(pif.URL);

                request.Credentials = CredentialCache.DefaultCredentials;
                request.UserAgent = "SoftwareRepository";
                request.Accept = "application/json";
                request.Method = "GET";
                // Get the response.
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // Display the status.
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                logIt("", responseFromServer, false);

                dynamic dynObj = JsonConvert.DeserializeObject(responseFromServer);
                pif.DownloadURL = dynObj.url;
                pif.fileSize = dynObj.fileSize;
                foreach (var chsum in dynObj.checksum)
                {
                    if(String.Compare("MD5", (string)chsum.type, true)==0)
                    {
                        pif.checktype = "MD5";
                        pif.checksum = chsum.value;
                        break;
                    }
                }
                g_downfiles.Enqueue(pif);
                reader.Close();
                dataStream.Close();
                response.Close();

            }catch(Exception e)
            {
                logIt("", e.ToString(), false);
            }
            Interlocked.Decrement(ref ThreadCount);
        }

        static void GetPhonePackList(string pdstr, String sTag)
        {
            try
            {
                // Create a request using a URL that can receive a post. 
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://api.swrepository.com/rest-api/discovery/1/package");

                request.Credentials = CredentialCache.DefaultCredentials;
                request.UserAgent = "SoftwareRepository";
                request.Accept = "application/json";
                request.ContentType = "application/json; charset=utf-8";
                request.KeepAlive = true;
                request.ServicePoint.Expect100Continue = true;
                // request.Connection = "Keep-Alive";
                //request.Expect = "100-continue";
                // Set the Method property of the request to POST.
                request.Method = "POST";
                // Create POST data and convert it to a byte array.
                string postData = pdstr;//"{\"api-version\":\"1\",\"condition\":[\"default\"],\"query\":{\"manufacturerHardwareModel\":\"0P6B2100\",\"manufacturerHardwareVariant\":\"VZW__001\",\"manufacturerName\":\"HTC\",\"manufacturerProductLine\":\"WindowsPhone\",\"packageClass\":\"Public\",\"packageType\":\"VariantSoftware\"},\"response\":null}";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
                // Get the response.
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                // Display the status.
                logIt(sTag, "List file:" + ((HttpWebResponse)response).StatusDescription, true);
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                // Display the content.
                logIt("",responseFromServer,false);
                dynamic dynObj = JsonConvert.DeserializeObject(responseFromServer);
                foreach (var data in dynObj.softwarePackages)
                {
                    String sID = data.id;
                    foreach (var file in data.files)
                    {
                        //Console.WriteLine(file.fileSize);
                        //Console.WriteLine(file.fileType);
                        //Console.WriteLine(string.Format("http://api.swrepository.com/rest-api/discovery/1/package/{0}/file/{1}/urls", sID, file.fileName));
                        //Console.WriteLine(file.checksum.value);
                        //Console.WriteLine(file.checksum.type);
                        PackageFileInfo pfi = new PackageFileInfo(sID);
                        pfi.fileSize = file.fileSize;
                        pfi.filetype = file.fileType;
                        pfi.filename = file.fileName;
                        pfi.checksum = file.checksum[0].value;
                        pfi.checktype = file.checksum[0].type;
                        pfi.sVersion = data.packageRevision;
                        pfi.URL = string.Format("http://api.swrepository.com/rest-api/discovery/1/package/{0}/file/{1}/urls", sID, file.fileName);
                        pfi.currentDir = Path.Combine(sTargetDir,sTag, pfi.sVersion);
                        if (!Directory.Exists(pfi.currentDir))
                        {
                            try
                            {
                                Directory.CreateDirectory(pfi.currentDir);
                            }
                            catch (Exception e)
                            {
                                logIt("", e.ToString(), false);
                            }
                        }
                        //g_fpinfo.Enqueue(pfi);
                        ThreadPool.QueueUserWorkItem(GetDownloadURL, pfi);
                    }
                }
                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();

            }
            catch(Exception e)
            {
                logIt(sTag, "List file:" + e.ToString(), true);
            }
        }

        static System.Configuration.Install.InstallContext _args = null;
        public static string sTargetDir = @"J:\temp\WinPhoneFW";

        public static void logIt(string sTag, string ss, bool bconsole)
        {
            try
            {
                string s = ss;
                if(!string.IsNullOrEmpty(sTag))
                {
                    s = string.Format("[{0}]:{1}", sTag, ss);
                }
                System.Diagnostics.Trace.WriteLine(s);
                if(bconsole)
                    System.Console.WriteLine(s);
            }
            catch (Exception) { }
        }

        static void ConCurrentDownfilelist()
        {
            PackageFileInfo localValue;
            if (g_downfiles.IsEmpty && ThreadCount > 0)
            {
                Thread.Sleep(1000);
            }
            if (g_downfiles.IsEmpty)
            {
                logIt("Not start", "Can not get download files list.", true);
                return;
            }
            do
            {
                while (!g_downfiles.IsEmpty && g_downfiles.TryDequeue(out localValue))
                {
                    ThreadPool.QueueUserWorkItem(DownLoadFiles.DownloadFile, localValue);
                }
                Thread.Sleep(1000);
            }while(ThreadCount > 0);
        }

        static void Downfilelist()
        {
            PackageFileInfo localValue;
            if (g_downfiles.IsEmpty && ThreadCount > 0)
            {
                Thread.Sleep(1000);
            }
            if (g_downfiles.IsEmpty)
            {
                logIt("Not start", "Can not get download files list.", true);
                return;
            }

            while (!g_downfiles.IsEmpty && g_downfiles.TryDequeue(out localValue))
            {
                DownLoadFiles.DownloadFile(localValue);
            }
        }

        static void Main(string[] args)
        {
            _args = new System.Configuration.Install.InstallContext(null, args);
            sTargetDir = AppDomain.CurrentDomain.BaseDirectory;
            string sconfigfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wpdlconfig.json");
            if(_args.Parameters.ContainsKey("config"))
            {
                sconfigfile = Environment.ExpandEnvironmentVariables(_args.Parameters["config"]);
            }
            if(_args.IsParameterTrue("help"))
            {
                Console.WriteLine("-help ");
                Console.WriteLine("-config=file config path\\wpdlconfig.json");
                Console.WriteLine("[-concurrent]");
                Console.WriteLine("-dir=download target dir");
            }
            if(_args.Parameters.ContainsKey("dir"))
            {
                sTargetDir = Environment.ExpandEnvironmentVariables(_args.Parameters["dir"]);
                if(!Directory.Exists(sTargetDir))
                {
                    try {
                        Directory.CreateDirectory(sTargetDir);
                    }
                    catch (Exception e)
                    {
                        logIt("Not start", "List file:" + e.ToString(), true);
                    }
                }
            }
            dynamic dynObj = JsonConvert.DeserializeObject(File.ReadAllText(sconfigfile));
            foreach(var phone in dynObj.config)
            {
                logIt("Model", (string)phone.phone, true);
                logIt((string)phone.phone, phone.query.ToString(), true);
                
                GetPhonePackList(phone.query.ToString(), (string)phone.phone);
            }
            Thread.Sleep(1000);
            if (_args.IsParameterTrue("concurrent"))
            {
                ConCurrentDownfilelist();
            }
            else 
            { 
                Downfilelist(); 
            }
        }
    }
}
