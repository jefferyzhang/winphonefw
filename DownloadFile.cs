﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using WinPhoneFW;
using System.Threading;

namespace XcDownLoadFile
{
    public class DownLoadFiles
    {
        static public String CalcFileMD5(string filename)
        {
            String s = "";
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    byte[] data= md5.ComputeHash(stream);
                    s = Convert.ToBase64String(data);
                }
            }
            return s;
        }

        ///
        /// 下载文件方法
        /// 
        /// 文件保存路径和文件名
        /// 返回服务器文件名
        ///
        static public void DownloadFile(Object obj)
        {
            Interlocked.Add(ref Program.ThreadCount, 1);
            PackageFileInfo pfi = (PackageFileInfo)obj;
            string strFileName = Path.Combine(pfi.currentDir, pfi.filename);
            string URL = pfi.DownloadURL;
            String chsum = pfi.checksum;

            // 打开上次下载的文件
            long SPosition = 0;
            // 实例化流对象
            FileStream FStream;
            // 判断要下载的文件夹是否存在
            if (File.Exists(strFileName))
            {
                string md5 = CalcFileMD5(strFileName);
                if (string.Compare(md5, chsum, true) == 0)
                {
                    Program.logIt(pfi.filename, "DownLoad Success.", true);
                    Interlocked.Decrement(ref Program.ThreadCount);
                    return;
                }
                // 打开要下载的文件
                FStream = File.OpenWrite(strFileName);
                // 获取已经下载的长度
                SPosition = FStream.Length;
                if (SPosition >= pfi.fileSize)
                {
                    SPosition = 0;
                    FStream.SetLength(0);
                }
                FStream.Seek(SPosition, SeekOrigin.Current);
            }
            else
            {
                // 文件不保存创建一个文件
                FStream = new FileStream(strFileName, FileMode.Create);
                SPosition = 0;
            }
            try
            {
                // 打开网络连接
                HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(URL);
                myRequest.KeepAlive = true;
                if (SPosition > 0)
                    myRequest.AddRange((int)SPosition);             // 设置Range值
                // 向服务器请求,获得服务器的回应数据流
                Stream myStream = myRequest.GetResponse().GetResponseStream();
                // 定义一个字节数据
                byte[] btContent = new byte[0x80000];
                pfi.cursize = SPosition;
                int intSize = myStream.Read(btContent, 0, 0x80000);
                while (intSize > 0)
                {
                    FStream.Write(btContent, 0, intSize);
                    intSize = myStream.Read(btContent, 0, 0x80000);
                    pfi.cursize+=intSize;
                    Program.logIt(pfi.filename, string.Format("DownLoad {0}% progress :{1}", ((pfi.cursize * 100.0)/pfi.fileSize), pfi.cursize), true);
                }
                // 关闭流
                FStream.Close();
                myStream.Close();

                string md5 = CalcFileMD5(strFileName);
                if (string.Compare(md5, chsum, true) == 0)
                {
                    Program.logIt(pfi.filename, "DownLoad Success.", true);
                }
                else
                {
                    Program.logIt(pfi.filename, "DownLoad failed. MD5 Check failed.", true);
                    Program.logIt(pfi.filename, "MD5 Check failed. Remove the file", true);
                    try
                    {
                        File.Delete(strFileName);
                    }catch(Exception e)
                    {
                        Program.logIt("", e.ToString(), false);
                    }
                }
            }
            catch (Exception e)
            {
                FStream.Close();
                Program.logIt(pfi.filename, "DownLoad failed. Exception.", true);
                Program.logIt("", e.ToString(), false);
            }
            Interlocked.Decrement(ref Program.ThreadCount);
            return ;
        }
    }
}